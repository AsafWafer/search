#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>

#include "main.h"
#include "squares.h"

////////////////////////////////////////////////////////////////////////////////
/// // globals /////////////////////////////////////////////////////////////////
/// ///////////////////////////////////////////////////////////////////////////
int vals[N][N];
int valsMAX[N][N];
int maxGain = 0;
static long long n = 0;

////////////////////////////////////////////////////////////////////////////////
/// // headers /////////////////////////////////////////////////////////////////
/// ///////////////////////////////////////////////////////////////////////////
void binSearch(int row, int col);
void printArray(int printMAX);

////////////////////////////////////////////////////////////////////////////////
/// // Mockups /////////////////////////////////////////////////////////////////
/// ///////////////////////////////////////////////////////////////////////////
int ant_pushTxVoltagesToAntenna(void *inputVoltages) {
	return 1;
}

int getGain() {
	return 1;
}

//////////////////////////////////////////////////////
/// Helpers
/// //////////////////////////////////////////////////
void copyArray(bool toMAX) {

	for (int row = 0; row < N; ++row) {
		for (int col = 0; col < N; ++col) {
			if (toMAX)
				valsMAX[row][col] = vals[row][col];
			else
				vals[row][col] = valsMAX[row][col];
		}
	}
}

void printArray(int printMAX) {
	//printf ("printArray\n");

	if (!printMAX) {
		printf("vals\n");

		for (int row = 0; row < N; ++row) {
			for (int col = 0; col < N; ++col) {
				printf("%d ", vals[row][col]);
			}
			printf("\n");
		}
	} else {
		printf("valsMAX"
				"\n");

		for (int row = 0; row < N; ++row) {
			for (int col = 0; col < N; ++col) {
				printf("%d ", valsMAX[row][col]);
			}
			printf("\n");
		}
	}
}

int printTime() {
	time_t timer;
	char buffer[26];
	struct tm* tm_info;

	time(&timer);
	tm_info = localtime(&timer);

	strftime(buffer, 26, "%Y-%m-%d %H:%M:%S", tm_info);
	puts(buffer);

	return 0;
}

//////////////////////////////////////////////////////////////////
/// binSearch
/// /////////////////////////////////////////////////////////////
void binSearch(int row, int col) {
	printf("binSearch row=%d col=%d \n", row, col);
	// loop for every val on this position
	for (int val = 0; val < MAX_VAL; ++val) {

		vals[row][col] = val;

		ant_pushTxVoltagesToAntenna(NULL);
		int gain = 0; // rand();
		printArray(0);
		// count iteration
		n++;

//		printf("binSearch %d %d %d : gain=%d\n",row,col,val,gain);

		if (gain > maxGain) {
			maxGain = gain;
			copyArray(true);
			printf("max gain:%d\n", maxGain);
			printArray(0);
		}

		// search next row
		if (row < N - 1)
			binSearch(row + 1, col);

		// search next col
		if (col < N - 1)
			binSearch(row, col + 1);

	}

	return;
}

//////////////////////////////////////////////////////////////////
/// binSearchSingleElement
///
/// recursive search over all element - golden solution
/// /////////////////////////////////////////////////////////////
void binSearchSingleElement(int i) {
	//printf("binSearch i=%d \n",i);
	// loop for every val on this position
	for (int val = 0; val < MAX_VAL; ++val) {

		vals[i / N][i % N] = val;

		// search next element
		if (i < N * N - 1)
			binSearchSingleElement(i + 1);
		else {

			//printf("binSearch i=%d val=%d \n",i,val);

			ant_pushTxVoltagesToAntenna(NULL);
			int gain = 0;	// rand();
			//printArray(0);

			// count iteration
			n++;

			//		printf("binSearch %d %d %d : gain=%d\n",row,col,val,gain);

			if (gain > maxGain) {
				maxGain = gain;
				copyArray(true);
				printf("max gain:%d\n", maxGain);
				printArray(0);
			}
		}

	}

	return;
}

//////////////////////////////////////////////////////////////////
/// binSearchSingleRow
///
/// iterative search of better measured Gain
/// loop through range of values - assign and measure
/// if the measured Gain has been improved - assign the new values and return true
/// Inputs:
/// 	Row: the row number
/// 	exitOnDegradation: do not continue if the gain drops after it has been improved
/// /////////////////////////////////////////////////////////////
bool binSearchSingleRow(int row, bool exitOnDegradation) {
	bool gainImproved = false;

	//printf("binSearch i=%d \n",i);
	// loop for every val on this position
	for (int val = MIN_VAL; val < MAX_VAL; val += STEP_VAL) {

		n++;

		for (int col = 0; col < N; ++col) {
			vals[row][col] = val;
		}

		// apply & measure
		ant_pushTxVoltagesToAntenna(NULL);
		int gain = rand();   //0;	//

		// check if improved
		if (gain > maxGain) {
			gainImproved = true;
			maxGain = gain;
			copyArray(true);
			printf("new max gain found:%d\n", maxGain);
			printArray(0);
		}
		else if (gainImproved && exitOnDegradation){
			printf("exitOnDegradation\n", maxGain);
			return true;
		}

	}

	return gainImproved;
}

//////////////////////////////////////////////////////////////////
/// searchRows
///
/// iterative search of better measured Gain
/// loop through rows - if improved re-loop
/// continue while the measured Gain has been improved - assign the valMAX to vals after every improvments
/// Inputs:
/// 	exitOnDegradation: do not continue if the gain drops after it has been improved
/// /////////////////////////////////////////////////////////////
void searchRows(bool exitOnDegradation) {

	bool gainImproved = false;


	do {
		gainImproved = false;
		for (int row = 0; row < N; ++row) {
			if (binSearchSingleRow(row, exitOnDegradation)){
				gainImproved = true;
				copyArray(false);
			}
		}

		// restart the search iff gainImproved
	} while (gainImproved);
}




////////////////////////////////////////////////////////////////////////////////
/// // main /////////////////////////////////////////////////////////////////
/// ///////////////////////////////////////////////////////////////////////////
int main(void) {

	// start rand
	srand(time(0));

	// start
	printf("Search on %d*%d Started & val range size is %d\n", N, N,
			(MAX_VAL-MIN_VAL)/STEP_VAL);
	printTime();

	// search binary squares
//	square s = {.size=N, .startPosCol=0, .startPosRow=0};
//	binSearchSquares(s);

	// search travelling square
	square s = {.size=MIN_SQUARE_SIZE, .startPosCol=0, .startPosRow=0};
	traverllingSquare(s);

	//searchRows(true);
	//binSearchSingleElement(0);
	//binSearch(0, 0);

	// finish
	printf("Search on %d*%d Finished\n", N, N);
	printTime();

	printf("Search domain size is %lld\n", n);

	return 0;
}
