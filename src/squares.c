/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Squares
/// /////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>

#include "squares.h"
#include "main.h"

square getQuarter(square s,quarterPos q){
	square qs;

	qs.size = s.size / 2;

	switch (q){
	case quarterPos_UpperLeft:
		// no need to change a thing in the new square - just the size changes
		qs.startPosCol = s.startPosCol;
		qs.startPosRow = s.startPosRow;
		break;
	case quarterPos_UpperRight:
		qs.startPosCol = s.startPosCol + qs.size;
		qs.startPosRow = s.startPosRow;
		break;
	case quarterPos_LowerLeft:
		qs.startPosCol = s.startPosCol;
		qs.startPosRow = s.startPosRow + qs.size;
		break;
	case quarterPos_LowerRight:
		qs.startPosCol = s.startPosCol + qs.size;
		qs.startPosRow = s.startPosRow + qs.size;
		break;
	}

	return qs;
}

bool posInSquare(int row, int col, square s){
	//check on rows
	if (row < s.startPosRow)
		return false;
	if (row >= s.startPosRow + s.size)
		return false;
	// check on cols
	if (col < s.startPosCol)
		return false;
	if (col >= s.startPosCol + s.size)
		return false;

	return true;

}

void printSquareInFullArray(square s){

	printf("Square is %d*%d starting at %d,%d\n", s.size, s.size, s.startPosRow, s.startPosCol);
	for (int row = 0; row < N; ++row) {
		for (int col = 0; col < N; ++col) {
			if (posInSquare(row,col,s))
				printf("X ");
			else
				printf("- ");
		}
		printf("\n");
	}
}

void binSearchSquares(square s){
	printf("binSearchSquares with %d*%d starting at %d,%d\n", s.size, s.size, s.startPosRow, s.startPosCol);

	if (s.size > MIN_SQUARE_SIZE)
		for (int q = 1; q <= 4; ++q) {
			// create the quarter
			square qs;
			qs = getQuarter(s,q);
			// search this quarter
			binSearchSquares(qs);
		}
	else
		printSquareInFullArray(s);
}


void traverllingSquare(square s){
	printf("traverllingSquare of %d*%d inside %d*%d\n", s.size, s.size, N, N);

	for (int row = 0; row <= N - s.size; ++row)
		for (int col = 0; col <= N - s.size; ++col) {
			s.startPosCol = col;
			s.startPosRow = row;
			printSquareInFullArray(s);
		}
}

